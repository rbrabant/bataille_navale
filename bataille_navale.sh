#!/bin/bash

source demarrage.sh
source coordonnee.sh
source grille.sh
source afficher_grille.sh
source demander_tir.sh
source mise_a_jour.sh
source coule.sh
source touche.sh

# Démarrage du jeu
demarrage

# Placement des bateaux
./bateaux.sh

toucher_fixme() {
    echo 'A'
}

partieTermine=0
while [ $partieTermine -ne 1 ]
do
    #victoire
    #partieTermine=$?
    
    tourJoueur1=1
    while [ $tourJoueur1 == 1 ]
    do
        joueur=1
        echo "Tour du joueur 1"
        demander_tir $joueur_1
        touche=`touche $joueur $ligne $colonne`

        if [ $touche == '0' ]
        then
            changer_valeur $joueur affichage $last_row $last_col 0
        else
            changer_valeur $joueur affichage $last_row $last_col 1

            idBateau=$touche
            estCoule=`coule $joueur $idBateau`
            if [ coule ]
            then
                for i in `seq 0 224`
                do
                    charbateau=${bateaux_joueur1[$i]}
                    if [ $charbateau == $idBateau ]
                    then
                        affichage_joueur1[$i]='2'
                    fi
                done
            fi
        fi
    
        clear
        echo "Voici la grille après le tir :"
		afficher_grille "${affichage_joueur1[@]}"
        read -p "Appuyez sur entrée pour passez au tour suivant..."

        # Il faut tester la victoire
        tourJoueur1=0
    done

    clear
    tourJoueur2=1
    while [ $tourJoueur2 == 1 ]
    do
        joueur=1
        echo "Tour du joueur 2"
        demander_tir $joueur_2
        touche=`touche $joueur $ligne $colonne`

        if [ $touche == '0' ]
        then
            changer_valeur $joueur affichage $last_row $last_col 0
        else
            changer_valeur $joueur affichage $last_row $last_col 1

            idBateau=$touche
            estCoule=`coule $joueur $idBateau`
            if [ coule ]
            then
                for i in `seq 0 224`
                do
                    charbateau=${bateaux_joueur2[$i]}
                    if [ $charbateau == $idBateau ]
                    then
                        affichage_joueur2[$i]='2'
                    fi
                done
            fi
        fi
    
        clear
        echo "Voici la grille après le tir :"
		afficher_grille "${affichage_joueur2[@]}"
        read -p "Appuyez sur entrée pour passez au tour suivant..."

        tourJoueur2=0
    done
    
    victoire
    partieTermine=$?
done

