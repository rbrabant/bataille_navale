#!/bin/bash

# initialisation des grilles
bateaux_joueur1=( 0 )
bateaux_joueur2=( 0 )
affichage_joueur1=( 0 )
affichage_joueur2=( 0 )

# On remplie les grilles
for case in `seq 1 224`
do
        bateaux_joueur1[$case]=0
        bateaux_joueur2[$case]=0
        affichage_joueur1[$case]=0
        affichage_joueur2[$case]=0
done

# echo ${bateaux_joueur1[*]}
# echo ${bateaux_joueur2[*]}
# echo ${affichage_joueur1[*]}
# echo ${affichage_joueur2[*]}

