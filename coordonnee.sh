#!/bin/bash

# $1 grille
# $2 ligne
# $3 colonne
coordonnees_to_valeur()
{
    local x=$1
    local y=$2
    shift
    shift
    local grille=("${@}")



    #echo $1
    #echo $3
    coordonnees=$(( $x*15+$y ))

    #echo $coordonnees
    echo ${grille[$coordonnees]}

}

#$1 grille d'affichage
#$2 ligne
#$3 colonne
#$4 valeur
changer_valeur()
{
   local nbr_joueur=$1
   local type_grid=$2
   local line=$3
   local col=$4
   local val=$5

   coordonnees=$(( $line*15+$col ))

   if [ $nbr_joueur == 1 ] && [ $type_grid == affichage ]
   then

       affichage_joueur1[$coordonnees]=$val

   elif [ $nbr_joueur == 1 ] && [ $type_grid == bateaux ]
   then
        bateaux_joueur1[$coordonnees]=$val

    elif [ $type_grid == bateaux ]
    then
       bateaux_joueur2[$coordonnees]=$val
    else
      affichage_joueur2[$coordonnees]=$val
    fi
}


#coordonnees_to_valeur 0 2 "${affichage_joueur1[@]}"
#changer_valeur 1 affichage 6 6 V
#echo ${affichage_joueur1[*]}
