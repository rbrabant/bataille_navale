#!/bin/bash

afficher_grille() {
	local grille=("$@")
	local taille=15

	echo '    A B C D E F G H I J K L M N O'
	echo '    -----------------------------'

	y=0
	for y in `seq 0 $((taille-1))`
	do
		echo -n $(($y+1)) # Début de la ligne
		if [ $y -lt 9 ]
		then
			echo -n ' | '
		else
			echo -n '| '
		fi

		# affichage de la ligne
		for x in `seq 0 $(($taille-1))`
		do
			index=$(($x+$taille*$y))
			casechar="${grille[$index]}"
			echo -n "$casechar "
		done

		echo ''
	done
	echo ''
}