#!/bin/bash

function get_coord
{
    ligne=$(($1 - 1))
    chaine="ABCDEFGHIJKLMNO"
    searchstring=`echo $2 | tr [:lower:] [:upper:]`
    rest=${chaine#*$searchstring}
    colonne=$(( ${#chaine} - ${#rest} - ${#searchstring} ))
    echo "$ligne $colonne"
}

#get_coord 1 H
