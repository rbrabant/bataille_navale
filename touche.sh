
touche()
#Prend en paramètres dans l'ordre : le numéro du joueur la ligne puis la colonne des coordonéees.
#Renvoie si on a touché un bâteau ou non.
{
	if [ $1 -eq 1 ]
	then
		chiffre_tableau=$(coordonnees_to_valeur $2 $3 "${affichage_joueur1[@]}")

		if [ $chiffre_tableau -ne 0 ]
		then
			indice_bateau=$chiffre_tableau]
			echo $indice_bateau


		else
			echo 0
		fi

	else 
		chiffre_tableau=$(coordonnees_to_valeur $2 $3 "${affichage_joueur2[@]}")

		if [ $chiffre_tableau -ne 0 ]
		then
			indice_bateau=$chiffre_tableau
			echo $indice_bateau


		else
			echo 0
		fi
		
	fi

}
