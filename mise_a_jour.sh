
function maj
#Met à jour les grilles
# maj joueur boolen_coule
{
	if [ $joueur == 1 ]
	local chiffre_tableau
	then
		chiffre_tableau=$(coordonnees_to_valeur last_row last_col "${affichage_joueur1[@]}")
	else
		chiffre_tableau=$(coordonnees_to_valeur last_row last_col "${affichage_joueur1[@]}")
	fi
	local joueur=$1
	local coule=$2

	if [ joueur -eq 1 ]
	then
		if [ $chiffre_tableau -ne 0 ]
		then
			local indice_bateau=$chiffre_tableau
			if [ $coule -eq 1 ]
			then
				for case in `seq 0 224`
				do
					if [ $bateaux_joueur1[$case] -eq $indice_bateau ]
					then 
						$affichage_joueur1[$case]=2
					fi
				done
			else 
				$affichage_joueur1[$chiffre_tableau]=1
			fi
		else
			$affichage_joueur1[$chiffre_tableau]=0
		fi
	else 
		if [ $bateaux_joueur2[$chiffre_tableau] -ne 0 ]
		then
			local indice_bateau=$bateaux_joueur2[$chiffre_tableau]		
			if [ $coule -eq 1 ]
			then
				for case in `seq 0 224`
				do
					if [ $bateaux_joueur2[$chiffre_tableau] -eq $indice_bateau ]
					then 
						$affichage_joueur2[$case]=2
					fi
				done
			else 
				$affichage_joueur2[$chiffre_tableau]=1
			fi
		else
			$affichage_joueur2[$chiffre_tableau]=0
		fi		
	fi

}