#!/bin/bash

source get_coord.sh
source afficher_grille.sh
source coordonnee.sh  

# initialisation des grilles
bateaux_joueur1=( 0 )
bateaux_joueur2=( 0 )

# On remplie les grilles
for case in `seq 1 224`
do
        bateaux_joueur1[$case]=0
        bateaux_joueur2[$case]=0
done

#echo ${bateaux_joueur1[*]}
#echo ${bateaux_joueur2[*]}


for joueur in 1 2; do
  if [ $joueur == 1 ]
    then 
      grille=bateaux_joueur1
  else 
    grille=bateaux_joueur2
  fi
  if [ $joueur == 1 ]
    then
      echo ${bateaux_joueur1[*]}
  else 
    echo ${bateaux_joueur2[*]}
  fi
  echo "Vous avez le droit à: -2 bateaux de 1 case /n -2 bateaux de 2 cases /n -2 bateaux de 3 cases /n -1 bateau de 4 cases /n -2 bateaux de 5 cases "
  echo "Joueur $joueur, placez vos bateaux :"
  places=0
  total=9
  while [[ $places -lt $total ]]; do
    for i in 1 1 2 2 3 4 5; do 
      declare -a tableau=( "A" "B" "C" "D" "E" "F" "G" "H" "I")
      k=0
      echo "Placez un bateau de $i case(s)"
      read -p "Entrer la ligne (en nombre entier) et la colonne (en lettre majuscule) de la première case (exemple: 1 A ): " r c
      resultat=$(get_coord $r $c)
      row=${resultat:0:1}
      col=${resultat:2:1}
      choix=False
      while [ $choix == "False" ]; do
          if [ $joueur == 1 ]
            then 
              #case=coordonnees_to_valeur $row $col "${bateaux_joueur1[@]}"
              indice=$(( $row*15 + $col ))
              case=${bateaux_joueur1[$indice]}
              echo $case
          else 
            #case=coordonnees_to_valeur $row $col "${bateaux_joueur2[@]}"
            indice=$(( $row*15 + $col ))
            case=${bateaux_joueur2[$indice]}
            echo $case 
          fi
          if [ $case == "0" ]
              then
                val=${tableau[$k]}
                case=val
                #changer_valeur $joueur bateau $row $col $val
                places=$(( $places + 1 ))
                choix="True"
                echo "Bateau placé. Il vous reste $(( $total - $places )) bateau(x) à placer."
          else
            read -p "Cette position est déjà prise, veuillez en choisir une autre:" r2 c2 
            row=$r2
            col=$c2
          fi
        done
      if [ ! $i -eq 1 ]
        then
        indice=$(( $row*15 + $col ))
        case=${grille[$indice]}
        val=${tableau[$k]}
        case=val
        echo $case
        read -p "Voulez-vous placer le bateau verticalement (entrez v) ou horizontalement (entrez h)?" direction
        if [ $direction == "v" ] 
          then 
            for j in `seq 2 $i`; do 
              read -p "Entrez la ligne de l'autre case :" ligne
              res=$(get_coord $ligne $col)
              $ligne=${res[0]}
              case=$(coordonnees_to_valeur $ligne $col "${bateaux_joueur$joueur[@]}")
              echo $case
              choix="False"

              while [ $choix == "False" ]; do
                if [ $joueur == 1 ]
                  then 
                    #case=coordonnees_to_valeur $ligne $col "${bateaux_joueur1[@]}"
                    indice=$(( $ligne*15 + $col ))
                    case=${bateaux_joueur1[$indice]}
                    echo $case
                else 
                  #case=coordonnees_to_valeur $ligne $col "${bateaux_joueur2[@]}"
                  indice=$(( $ligne*15 + $col ))
                  case=${bateaux_joueur2[$indice]}
                  echo $case
                fi
                if [ $case == "0" ]
                  then
                    val=${tableau[$k]}
                    #changer_valeur $joueur bateau $ligne $col $val
                    case=val
                    places=$(( $places + 1 ))
                    choix="True"
                    echo "Bateau placé. Il vous reste $(( $total - $places )) bateau(x) à placer."
                else
                  read -p "Cette position est déjà prise, veuillez en choisir une autre: " r2 c2
                  ligne=$r2
                  col=$c2
                fi
              done
            done
        elif [ $direction == "h" ] 
          then 
            for j in `seq 2 $i`; do
              read -p "Entrez la colonne de l'autre case :" column
              res=$(get_coord $row $column)
              $column=${res[2]}
              choix="False"
              while [ $choix=="False" ]; do
                if [ $joueur == 1 ]
                  then 
                    #case=coordonnees_to_valeur $row $column "${bateaux_joueur1[@]}"
                    indice=$(( $row*15 + $column ))
                    case=${bateaux_joueur1[$indice]}
                    echo $case
                else 
                  #case=coordonnees_to_valeur $row $column "${bateaux_joueur2[@]}"
                  indice=$(( $row*15 + $column ))
                  case=${bateaux_joueur2[$indice]}
                  echo $case
                fi
                if [ $case == "0" ];
                  then
                    val=${tableau[$k]}
                    changer_valeur $joueur bateau $row $column $val
                    places=$(( $places + 1 ))
                    choix="True"
                    echo "Bateau placé. Il vous reste $(( $total - $places )) bateau(x) à placer."
                  else
                    read -p "Cette position est déjà prise, veuillez en choisir une autre: " r2 c2
                    row=$r2
                    column=$c2
                fi
              done
            done
        fi
      fi
      k=$(( $k+1 ))
      if [ $joueur == 1 ]
        then
          afficher_grille "${bateaux_joueur1[@]}"
      else 
          afficher_grille "${bateaux_joueur2[@]}"
      fi
    done  
  done
  read -p "Tapez suivant pour passer au deuxième joueur" suivant
  clear 
done

