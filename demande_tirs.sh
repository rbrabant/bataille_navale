#!/bin/bash

function demander_tir
{
	if [ $1 == $joueur_1 ]
	then
		afficher_grille "${affichage_joueur1[@]}"
	else
		afficher_grille "${affichage_joueur2[@]}"
	fi
	echo "$1, veuillez faire le choix de votre tir."
	while [ -z $ligne ] || (( $ligne < 1 )) || (( $ligne >= 15 ))
	do
		read -p "Numéro de ligne (entre 1 et 15) : " ligne
	done
	echo "Numéro de ligne enregistré : $ligne"

	while [ -z $colonne ] || [ ${#colonne} -ne 1 ] || [[ ! "ABCDEFGHIJKLMNOabcdefghijklmno" == *$colonne* ]]
	do
		read -p "Numéro de colonne (entre A et O) : " colonne
	done
	echo "Numéro de colonne enregistré  : $colonne"

		$last_row=$ligne
	$last_col=$colonne
}

#toucher $1 $ligne $colonne