#!/bin/bash

demarrage() {
	echo "Bienvenue au jeu de la bataille navale !

	Explication des règles du jeu :
	Le jeu de la bataille navale se joue avec 2 joueurs qui s'affronteront l'un l'autre. Chaque joueur aura des bateaux à placer. Il y aura :
	- 2 bateaux 1 case
	- 2 bateaux 2 cases
	- 1 bateaux 5 cases
	- 1 bateau 2x2 cases
	- 1 bateau 4 cases
	- 2 bateaux 3 cases

	Chaque joueur devra placer ses bateaux. Une fois les bateaux des deux joueurs placés, la partie peut commencer. La partie se déroule paer tour. Lors d'un tour, le joueur choisi une case sur le plateau de l'adversaire. Si un bateau est sur la case choisie, le bateau est 'Touché !' et le joueur a le droit de rejouer. Un bateau est 'Coulé !' quand toutes les cases d'un même bateau ont été touchées. Le tour est terminé lorsque la case choisie ne contient pas de bateau.
	Le vainqueur est le premier joueur ayant coulé l'ensemble des bateaux de son adversaire.


	"

	while [ -z $joueur_1 ]
	do
		read -p "Veuillez entrer le nom du joueur_1 : " joueur_1
	done
	echo "Bienvenue $joueur_1 !"

	while [ -z $joueur_2 ]
	do
		read -p "Veuillez entrer le nom du joueur_2 : " joueur_2
	done
	echo "Bienvenue $joueur_2 !"

}
