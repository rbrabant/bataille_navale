coule() {
    # Variable d'entrée : ID joueur, ID bateau
    # Booléan coulé
    # coule joueur idbateau

    local idBateau=$2
    local joueur=$1

    if [ $joueur==1 ]
    then
        for y in `seq 0 14`
        do
            for x in `seq 0 14`
            do
                local index=$(($x+15*$y))
			    local casecharInv="${$bateaux_joueur2[$index]}"

                # Pour chaque coordonnées du bâteau en question, on test si elle est touchée dans la grille visible
                if [ $casecharInv == "$idBateau" ]
                then
                    local casecharV="${$affichage_joueur1[$index]}"
                    if [ $casecharV != '1' ]
                    then
                        return 0 # Donc on a trouvé une case de ce bâteau qui n'est pas touché, donc il n'est pas coulé
                    fi
                fi
            done
        done
        return 1 # Finalement toutes les cases de ce bâteau sont touchées => le bâteau est coulé.
        score_joueur1+=1
    else
        for y in `seq 0 14`
        do
            for x in `seq 0 14`
            do
                local index=$(($x+15*$y))
			    local casecharInv="${$bateaux_joueur1[$index]}"

                # Pour chaque coordonnées du bâteau en question, on test si elle est touchée dans la grille visible
                if [ $casecharInv == "$idBateau" ]
                then
                    local casecharV="${$affichage_joueur2[$index]}"
                    if [ $casecharV != '1' ]
                    then
                        return 0 # Donc on a trouvé une case de ce bâteau qui n'est pas touché, donc il n'est pas coulé
                    fi
                fi
            done
        done
        return 1 # Finalement toutes les cases de ce bâteau sont touchées => le bâteau est coulé.
        score_joueur2+=1
    fi
    
    
}