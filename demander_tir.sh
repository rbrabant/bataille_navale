#!/bin/bash

last_col=0
last_row=0

# La fonction s'appelle avec le nom du joueur actuel
# Les coordonnées rentrées par le joueur seront récupérées dans last_col et last_row

function demander_tir
{
	joueur=$1

	if [ $joueur == $joueur_1 ]
	then
		afficher_grille $affichage_joueur1
	else
		afficher_grille $affichage_joueur2
	fi
	echo "$joueur, veuillez faire le choix de votre tir."
	while [ -z $ligne ] || (( $ligne < 1 )) || (( $ligne >= 15 ))
	do
		read -p "Numéro de ligne (entre 1 et 15) : " ligne
	done
	echo "Numéro de ligne enregistré : $ligne"

	while [ -z $colonne ] || [ ${#colonne} -ne 1 ] || [[ ! "ABCDEFGHIJKLMNOabcdefghijklmno" == *$colonne* ]]
	do
		read -p "Numéro de colonne (entre A et O) : " colonne
	done
	echo "Numéro de colonne enregistré  : $colonne"

	result=$(get_coord $ligne $colonne)
	$ligne=${result[0]}
	$colonne=${result[2]}

	$last_row=$ligne
	$last_col=$colonne
}