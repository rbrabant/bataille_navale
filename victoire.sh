#!/bin/bash

victoire() {
    grille=$1

}

coule() {
    # Variable d'entrée : ID du bâteau, grille visible, grille invisible
    # Booléan coulé

    idBateau=$1
    grilleVisible=$2
    grilleInvisible=$3

    for y in `seq 0 14`
    do
        for x in `seq 0 14`
        do
            index=$(($x+15*$y))
			casecharInv="${grilleInvisible[$index]}"

            # Pour chaque coordonnées du bâteau en question, on test si elle est touchée dans la grille visible
            if [ $casecharInv == "$idBateau" ]
            then
                casecharV="${grilleVisible[$index]}"
                if [ $casecharV != '1' ]
                then
                    return 0 # Donc on a trouvé une case de ce bâteau qui n'est pas touché, donc il n'est pas coulé
                fi
            fi
        done
    done
    
    return 1 # Finalement toutes les cases de ce bâteau sont touchées => le bâteau est coulé.
}