Code développé par : Paul, Arthur, Aliénor, Yasmine et Rémi. 
Fait le 03/02/2023

Ce programme simule un jeu de bataille navale !

Explication des règles du jeu :
Le jeu de la bataille navale se joue avec 2 joueurs qui s'affronteront l'un l'autre. Chaque joueur aura des bateaux à placer. Il y aura :
- 2 bateaux 1 case
- 2 bateaux 2 cases
- 2 bateaux 5 cases
- 1 bateau 4 cases
- 2 bateaux 3 cases

Chaque joueur devra placer ses bateaux. Une fois les bateaux des deux joueurs placés, la partie peut commencer. La partie se déroule paer tour. Lors d'un tour, le joueur choisi une case sur le plateau de l'adversaire. Si un bateau est sur la case choisie, le bateau est 'Touché !' et le joueur a le droit de rejouer. Un bateau est 'Coulé !' quand toutes les cases d'un même bateau ont été touchées. Le tour est terminé lorsque la case choisie ne contient pas de bateau.
Le vainqueur est le premier joueur ayant coulé l'ensemble des bateaux de son adversaire.

Représentation dans les matrices de données :
mer=0
touche=1
coule=2
plouf=3

Les bateax sont différenciés séparément par : A, B, C...
